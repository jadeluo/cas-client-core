**修改了部分代码，目的是支持登出操作，与在反向代理环境中，能够获取正确的请求信息**

1. 在 AuthenticationFilter 中 添加 做登出操作。

```java
  //  登出 start 。
        if(assertion != null && CommonUtils.isNotBlank(casServerLogoutUrl) ) {
            String url = request.getRequestURI();
            String contextpath = request.getContextPath();
            if (url.startsWith(contextpath)) {
                url = url.substring(contextpath.length() + 1);
            }

            logger.debug("request url : {}", url);

            if (url.equals(logoutUrl)) {
                // request.getSession().invalidate();
                final String serviceUrl = constructServiceUrl(request, response);
                String modifiedServiceUrl = serviceUrl.replace(url,"");

                logger.debug("logout url service url: {}", modifiedServiceUrl);

                final String urlToRedirectTo = CommonUtils.constructRedirectUrl(this.casServerLogoutUrl,
                        getProtocol().getServiceParameterName(), modifiedServiceUrl, this.renew, this.gateway);

                this.authenticationRedirectStrategy.redirect(request, response, urlToRedirectTo);
                return;
            }
        }
        //  登出 end 。
```

2. 在CommonUtils 中 添加,目的是，如果是代理的操作，那么获取头信息中的一下信息，并且

```java
    FORWARDED_HEADER_HOST = "x-forwarded-host";
    FORWARDED_HEADER_PORT = "x-forwarded-port";
    FORWARDED_HEADER_PROTO = "x-forwarded-proto";
```


###配置###
```xml
    <context-param>
        <param-name>serverName</param-name>
        <param-value>127.0.0.1 localhost</param-value>
    </context-param>
    <filter>
        <!-- cas 登出拦截器 -->
        <filter-name>CAS Single Sign Out Filter</filter-name>
        <filter-class>org.jasig.cas.client.session.SingleSignOutFilter</filter-class>
        <init-param>
            <param-name>casServerUrlPrefix</param-name>
            <param-value>http://127.0.0.1:8443/cas</param-value>
            <!-- cas server 端地址的配置。 这个主要是用户登出用的  -->
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>CAS Single Sign Out Filter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>



    <filter>
        <!-- 判断用户时候进行登录的，没有今登陆将跳转到casServerLoginUrl页面进行登录，这里手动实现了一下，用于登出 -->
        <filter-name>CAS Authentication Filter</filter-name>
        <filter-class>org.jasig.cas.client.authentication.AuthenticationFilter</filter-class>
        <init-param>
            <param-name>logoutUrl</param-name>
            <param-value>managerloginout.jsf</param-value>
        </init-param>
        <init-param>
            <param-name>casServerLoginUrl</param-name>
            <param-value>http://127.0.0.1:8443/cas/login</param-value>
        </init-param>
        <init-param>
            <param-name>casServerLogoutUrl</param-name>
            <param-value>http://127.0.0.1:8443/cas/logout</param-value>
        </init-param>
        <init-param>
            <param-name>ignorePattern</param-name>
            <param-value>^.*[.](js|css|gif|png|zip)$</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>CAS Authentication Filter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>




    <filter>
        <!-- 用于进行ticket认证 -->
        <filter-name>CAS Validation Filter</filter-name>
        <filter-class>org.jasig.cas.client.validation.Cas20ProxyReceivingTicketValidationFilter</filter-class>
        <init-param>
            <!-- ticket 认证的地址，这里配置的是内网地址-->
            <param-name>casServerUrlPrefix</param-name>
            <param-value>http://127.0.0.1:8443/cas</param-value>
        </init-param>
        <init-param>
            <param-name>redirectAfterValidation</param-name>
            <param-value>true</param-value>
        </init-param>
        <init-param>
            <param-name>acceptAnyProxy</param-name>
            <param-value>false</param-value>
        </init-param>
        <init-param>
            <param-name>useSession</param-name>
            <param-value>true</param-value>
        </init-param>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>utf-8</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>CAS Validation Filter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>



    <filter>
        <filter-name>CAS HttpServletRequest Wrapper Filter</filter-name>
        <filter-class>org.jasig.cas.client.util.HttpServletRequestWrapperFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>CAS HttpServletRequest Wrapper Filter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
```
 
    
    